// Imports
var gulp = require('gulp'); 
var sass = require('gulp-sass');
var styleguide = require('sc5-styleguide');

var styleguideTitle = 'Example Styleguide';

// Path definitions
var paths = (function () {
  var obj = {};

  obj.root = './';
  obj.src = obj.root + 'src/';
  obj.build = obj.root + 'build/';

  obj.html = obj.src + '/**/*.html';

  obj.templates = obj.root + 'templates/';

  obj.sass = obj.src + 'sass/';
  obj.css = obj.build + 'css/';

  obj.srcJS = obj.src + 'js/';
  obj.buildJS = obj.build + 'js/';
  obj.srcJSLibs = obj.srcJS + 'libs/';

  obj.images = obj.root + 'images/';
  obj.svg = obj.images + 'svg/';
  obj.sprites = obj.images + 'sprites/';

  obj.readme = obj.src + 'README.md';
  obj.styleguide = obj.root + 'styleguide/';

  return obj;
})();


// Building the application
gulp.task('html', function() {
    return gulp.src(paths.html)
        .pipe(gulp.dest(paths.build));
});

gulp.task('scss', function() {
    return gulp.src(paths.sass + 'styles.scss')
        .pipe(sass())
        .pipe(gulp.dest(paths.build + 'css'));
});


// Building styleguide for static hosting to be displayed as a part of the application
gulp.task('staticStyleguide:generate', function() {
  return gulp.src(paths.sass + '**/*.scss')
    .pipe(styleguide.generate({
        title: styleguideTitle,
        rootPath: paths.styleguide,
        appRoot: paths.styleguide,
        overviewPath: paths.readme
      }))
    .pipe(gulp.dest(paths.styleguide));
});

gulp.task('staticStyleguide:applystyles', function() {
  return gulp.src(paths.sass + 'styles.scss')
    .pipe(sass({
      errLogToConsole: true
    }))
    .pipe(styleguide.applyStyles())
    .pipe(gulp.dest(paths.styleguide));
});

gulp.task('staticStyleguide', ['staticStyleguide:generate', 'staticStyleguide:applystyles']);


// Running styleguide development server to view the styles while you are working on them
gulp.task('styleguide:generate', function() {
  return gulp.src(paths.sass + '**/*.scss')
    .pipe(styleguide.generate({
        title: styleguideTitle,
        server: true,
        rootPath: paths.styleguide,
        overviewPath: paths.readme
      }))
    .pipe(gulp.dest(paths.styleguide));
});

gulp.task('styleguide:applystyles', function() {
  return gulp.src(paths.sass + 'styles.scss')
    .pipe(sass({
      errLogToConsole: true
    }))
    .pipe(styleguide.applyStyles())
    .pipe(gulp.dest(paths.styleguide));
});

gulp.task('styleguide', ['styleguide:generate', 'styleguide:applystyles']);


// Developer mode
gulp.task('default', ['html', 'scss', 'styleguide'], function() {
    gulp.watch(paths.html, ['html']);
    gulp.watch(paths.sass + '**/*.scss', ['scss', 'styleguide']);
    console.log(
        '\nDeveloper mode!\n\nSC5 Styleguide available at http://localhost:3000/\n'
    );
});


// The basic build task
// gulp.task('default', ['html', 'scss', 'staticStyleguide'], function() {
//     console.log(
//         '\nBuild complete!\n\nFresh build available in directory: ' +
//         paths.build + '\n\nCheckout the build by commanding\n' +
//         '(cd ' + paths.build + '; python -m SimpleHTTPServer)\n' +
//         'and pointing yout browser at http://localhost:8000/\n' +
//         'or http://localhost:8000/styleguide/ for the styleguide\n\n' +
//         'Run gulp with "gulp dev" for developer mode and style guide!\n'
//     );
// });