# Styleguide Generator

## Creating a section
Creates a section as an overview, no code goes into this

```
// Section Title
//
// A description about this section
//
// Styleguide 1.0

```


## Creating a sub-section

```
// Section Sub-title
//
// Description
//
// markup:
// <div class="example">
//    <p>Hello world!</p>
// </div>
//
// :hover - Highlight the button when hovered
//
// Styleguide 1.1
```